# Mad Libs

Mad Libs é um jogo de palavras frasal de modelos em que um jogador solicita a outros uma lista de palavras para substituir espaços em branco em uma história antes de ler em voz alta. O jogo é frequentemente jogado como um jogo de festa ou como passatempo .

O jogo foi inventado nos Estados Unidos e mais de 110 milhões de cópias dos livros da Mad Libs foram vendidas desde que a série foi publicada pela primeira vez em 1958.
